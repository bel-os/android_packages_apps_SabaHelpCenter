LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
 

# Name of the APK to build
LOCAL_MODULE := SabaHelpCenter
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := SabaHelpCenter

shc_root	:= $(LOCAL_PATH)
shc_dir		:= app
shc_out		:= $(PWD)/$(OUT_DIR)/target/common/obj/APPS/$(LOCAL_MODULE)_intermediates
shc_build	:= $(shc_root)/$(shc_dir)/build
shc_apk		:= build/outputs/apk/debug/app-release.apk


$(shc_root)/$(shc_dir)/$(shc_apk):
	rm -rf $(shc_root)/build && rm -rf $(shc_root)/$(shc_dir)/build && rm -rf $(shc_root)/.gradle
	cd $(shc_root) && sed -i -e '/focusCompile/d' -e '/geckoCompile/d' -e '/Dynamically set versionCode/,/^}/d' build.gradle
	cd $(shc_root) && ./gradlew assembleRelease


# Build all java files in the java subdirectory
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := $(shc_dir)/$(shc_apk)
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
 
# Tell it to build an APK
include $(BUILD_PREBUILT)





 
